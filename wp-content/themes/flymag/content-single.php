<?php
/**
 * @package FlyMag
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

        <?php if ( get_theme_mod('flymag_single_date') != 1 ) : ?>
            <div class="entry-meta">


                <?php flymag_posted_on(); ?>

                <footer class="entry-footer">
                    <?php flymag_entry_footer(); ?>



                </footer><!-- .entry-footer -->


            </div><!-- .entry-meta -->




        <?php endif; ?>
    </header><!-- .entry-header -->

	<?php if ( has_post_thumbnail() ) : ?>
		<div class="single-thumb">
            <img src="<?php the_post_thumbnail_url('entry-thumb'); ?>">
		</div>	
	<?php endif; ?>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'flymag' ),
				'after'  => '</div>',
			) );
		?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->

<div id="article-author">
    <div>
        <?php
        $author_email = get_the_author_meta('email');
        echo '<a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_avatar($author_email,'75') . '</a>';
        ?>
    </div>
    <div>
        <p><strong>Автор статьи:</strong> <?php echo '<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author_meta('display_name') ) . '</a>'; ?></p>
        <p><?php the_author(); ?> опубликовал(a) статей: <?php the_author_posts(); ?></p>
        <p><?php the_author_meta('description');?></p>
    </div>
</div>