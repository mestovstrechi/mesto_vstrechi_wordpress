<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package FlyMag
 */

get_header(); ?>

	<?php if ( get_theme_mod('blog_layout', 'classic') == 'fullwidth' ) {
		$layout = 'fullwidth';
	} else {
		$layout = '';
	} ?>
	<?php if ( get_theme_mod('blog_layout', 'classic') == 'masonry' ) {
		$masonry = 'home-masonry';
	} else {
		$masonry = '';
	} ?>

	<div id="primary" class="content-area <?php echo $layout; ?>">
		<main id="main" class="site-main" role="main">
            <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
                <?php dynamic_sidebar( 'sidebar-2' ); ?>
            <?php endif; ?>
        </main><!-- #main -->
	</div><!-- #primary -->

<?php if ( get_theme_mod('blog_layout', 'classic') != 'fullwidth' ) {
	get_sidebar();
} ?>
<?php get_footer(); ?>
